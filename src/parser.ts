import { IParsedMessage, IrcMessageFactory } from "./message-factory";
import { IRC_TYPES } from "./message-types";

export enum PARSER_STEPS {
    RAW,
    TAGS,
    PREFIX,
    TYPE,
    PARAMS,
    CONTENT,
}

export class Parser {
    public static stripMessages(messages: string): string[] {
        const messageArray = messages.split("\n\r").filter((v) => v.length > 0);
        return messageArray;
    }
    public static parseMessage(message: string): IParsedMessage | undefined {
        if (message.length < 1) {
            return;
        }
        const msg = new IrcMessageFactory(message);
        let step = PARSER_STEPS.RAW;
        let canNext: boolean = true;
        const paramsFound = [];
        while (message.length) {
            canNext = true;
            // todo logic is wrong, should not go to next if verifier fails
            if (step === PARSER_STEPS.TAGS && !this.hasAtChar(message)) {
                // skip empty tags
                step = PARSER_STEPS.PREFIX;
                canNext = false;
            } else if (step === PARSER_STEPS.PREFIX && !this.hasColonChar(message)) {
                // skip empty prefix
                step = PARSER_STEPS.TYPE;
                canNext = false;
            } else if (step === PARSER_STEPS.PARAMS && this.hasColonChar(message)) {
                // skip empty params / ended params
                step = PARSER_STEPS.CONTENT;
                msg.params(paramsFound);
                canNext = false;
            } else if (step === PARSER_STEPS.TAGS && this.hasAtChar(message)) {
                // parse tags found
                msg.tags(this.parseTags(message));
                step = PARSER_STEPS.PREFIX;
            } else if (step === PARSER_STEPS.PREFIX && this.hasColonChar(message)) {
                // prefix exists
                msg.prefix(this.parsePrefix(message));
                step = PARSER_STEPS.TYPE;
            } else if (step === PARSER_STEPS.TYPE) {
                // type is always available
                msg.type(this.parseType(message));
                step = PARSER_STEPS.PARAMS;
            } else if (step === PARSER_STEPS.PARAMS) {
                const found = this.parseParam(message);
                paramsFound.push(found || "");
                if (!this.hasNext(message)) {
                    msg.params(paramsFound);
                }
            } else if (step === PARSER_STEPS.CONTENT) {
                msg.content(this.parseContent(message));
                message = "";
                canNext = false;
            } else {
                msg.type(IRC_TYPES.UNKNOWN);
                step = PARSER_STEPS.TAGS;
                canNext = false;
            }

            if (canNext) {
                message = this.getNext(message) || "";
            }
        }
        return msg.build();
    }
    public static hasAtChar(message: string): boolean {
        return message.charCodeAt(0) === 64;
    }
    public static parseTags(message: string): Map<string, string> {
        const tags = new Map<string, string>();
        message = this.getFirst(message) || "";

        if (!this.hasAtChar(message)) {
            return tags;
        }
        const tagsRaw = message.substr(1).split(";").filter((v) => v.length > 0);
        tagsRaw.forEach((v) => {
            const kv = v.split("=");
            tags.set(kv[0], kv[1].replace("\\s", " "));
        });
        return tags;
    }
    public static hasColonChar(message: string): boolean {
        return message.charCodeAt(0) === 58;
    }
    public static parsePrefix(message: string): Map<string, string> {
        const prefix = new Map<string, string>();
        const first = this.getFirst(message);
        if (!first || !this.hasColonChar(message)) {
            return prefix;
        }
        message = first.substr(1);
        const nickStopPos = message.indexOf("!");
        const hostStartPos = message.indexOf("@", 1);

        if (nickStopPos !== -1 && hostStartPos !== -1) {
            prefix.set("nick", message.substr(0, nickStopPos));
            prefix.set("user", message.substr(nickStopPos + 1, (hostStartPos - 1) - nickStopPos));
            prefix.set("host", message.substr(hostStartPos + 1));
        } else {
            prefix.set("host", message);
        }
        return prefix;
    }
    public static parseContent(message: string): string | undefined {
        if (!this.hasColonChar(message)) { return; }
        const content = message.substr(1).trim();
        return content.length > 0 ? content : undefined;
    }
    public static isPing(message: string): boolean {
        return message.startsWith("PING :");
    }
    public static isPong(message: string): boolean {
        return message.startsWith("PONG :");
    }
    public static parseParam(message: string): string | undefined {
        if (!this.hasColonChar(message)) {
            return this.getFirst(message);
        }
        return;
    }
    public static hasNext(message: string): boolean {
        return message.trim().indexOf(" ") !== -1;
    }
    public static getNext(message: string): string | undefined {
        const firstSpacePos = message.indexOf(" ");
        return firstSpacePos !== -1 ? message.substr(firstSpacePos + 1) : undefined;
    }
    public static getFirst(message: string): string|undefined {
        if (!message || message.length < 1) {
            return;
        }
        message = message.trim();
        if (!this.hasNext(message)) {
            return message;
        }
        return message.split(/\s+/).shift();
    }
    public static parseType(rawType: string|null): IRC_TYPES {
        if (rawType === null) {
            return IRC_TYPES.UNKNOWN;
        }
        rawType = this.getFirst(rawType) || "";
        if (!rawType || rawType.length < 1) {
            return IRC_TYPES.UNKNOWN;
        }
        switch (rawType) {
            case "376":
                return IRC_TYPES.CODE_376;
            case "CAP":
                return IRC_TYPES.CAP;
            case "JOIN":
                return IRC_TYPES.JOIN;
            case "PART":
                return IRC_TYPES.PART;
            case "PING":
                return IRC_TYPES.PING;
            case "PONG":
                return IRC_TYPES.PONG;
            case "PRIVMSG":
                return IRC_TYPES.PRIVMSG;
            case "ROOMSTATE":
                return IRC_TYPES.ROOMSTATE;
            case "USERNOTICE":
                return IRC_TYPES.USERNOTICE;
            case "USERSTATE":
                return IRC_TYPES.USERSTATE;
            case "CLEARCHAT":
                return IRC_TYPES.CLEARCHAT;
            case "CLEARMSG":
                return IRC_TYPES.CLEARMSG;
            case "NOTICE":
                return IRC_TYPES.NOTICE;
            case "HOSTTARGET":
                return IRC_TYPES.HOSTTARGET;
            case "GLOBALUSERSTATE":
                return IRC_TYPES.GLOBALUSERSTATE;
        }
        return IRC_TYPES.UNKNOWN;
    }
}

export default Parser;
