export enum IRC_TYPES {
    UNKNOWN,
    PING,
    PONG,
    PRIVMSG,
    JOIN,
    CAP,
    PART,
    NOTICE,
    ROOMSTATE,
    USERSTATE,
    USERNOTICE,
    HOSTTARGET,
    GLOBALUSERSTATE,
    CLEARCHAT,
    CLEARMSG,
    CODE_376,
}

export default { IRC_TYPES };
