import { IRC_TYPES } from "./message-types";

export interface IParsedMessage {
    raw: string;
    type: IRC_TYPES;
    tags: Map<string, string>;
    params: string[];
    prefix: Map<string, string>;
    content: string | undefined;
}
export class IrcMessageFactory {
    private pRaw: string|null = null;
    private pPrefix: Map<string, string>|null = null;
    private pParams: string[] = [];
    private pTags: Map<string, string>|null = null;
    private pContent: string|null = null;
    private pType: IRC_TYPES = IRC_TYPES.UNKNOWN;

    constructor(raw: string) {
        this.pRaw = raw;
        this.pParams = [];
        this.pPrefix = null;
        this.pTags = null;
        this.pType = IRC_TYPES.UNKNOWN;
        this.pContent = null;
    }
    public build(): IParsedMessage {
        return Object.assign({
            content: this.pContent,
            params: this.pParams,
            prefix: this.pPrefix,
            raw: this.pRaw,
            tags: this.pTags,
            type: this.pType,
        });
    }
    public tags(tags: Map<string, string>): IrcMessageFactory {
        this.pTags = tags;
        return this;
    }
    public prefix(prefix: Map<string, string>): IrcMessageFactory {
        this.pPrefix = prefix;
        return this;
    }
    public type(type: IRC_TYPES): IrcMessageFactory {
        this.pType = type;
        return this;
    }
    public content(content?: string): IrcMessageFactory {
        if (!content) {
            return this;
        }
        this.pContent = content;
        return this;
    }
    public params(content?: string[]): IrcMessageFactory {
        if (!content) {
            return this;
        }
        this.pParams = [
            ...content.filter((param) => param.length > 0),
        ];
        return this;
    }
}
