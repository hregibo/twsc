import { IRC_TYPES } from "./message-types";
import { Parser } from "./parser";

describe("stripMessages", () => {
    test("should divide the raw data into an array of 3", () => {
        const testString = "test1\n\rtest two\n\rtest 3";
        const result = Parser.stripMessages(testString);
        expect(result.length).toBeDefined();
        expect(result.length).toEqual(3);
    });
    test("should remove last line if empty", () => {
        const testString = "test1\n\r";
        const result = Parser.stripMessages(testString);
        expect(result.length).toEqual(1);
        expect(result[0]).toEqual("test1");
    });
    test("should remove first line if empty", () => {
        const testString = "\n\rtest1";
        const result = Parser.stripMessages(testString);
        expect(result.length).toEqual(1);
        expect(result[0]).toEqual("test1");
    });
    test("should return an empty array if no message is valid", () => {
        const testString = "\n\r\n\r\n\r";
        const result = Parser.stripMessages(testString);
        expect(result.length).toEqual(0);
    });
});
describe("hasAtChar", () => {
    test("should return true as the message starts with @", () => {
        expect(Parser.hasAtChar("@badges=vip,cheer150000;mod=0;display=Test")).toBeTruthy();
    });
    test("should return false as it starts with a server info", () => {
        expect(Parser.hasAtChar(":tmi@tmi.twitch.tv PRIVMSG #test :hello tester")).toBeFalsy();
    });
    test("should return false as it is a PING", () => {
        expect(Parser.hasAtChar("PING :tmi.twitch.tv")).toBeFalsy();
    });
});
describe("parseParam", () => {
    test("should return the parameter", () => {
        const wanted = "potato chips";
        const result = Parser.parseParam(wanted);

        expect(result).toEqual("potato");
    });
    test("should return undefined", () => {
        const wanted = ":chips";
        const result = Parser.parseParam(wanted);

        expect(result).toBeUndefined();
    });
});
describe("parseTags", () => {
    test("should return an empty object as it does not start with tags", () => {
        const str = ":tmi.twitch.tv #test :hello";
        const result = Parser.parseTags(str);
        expect(result.size).toEqual(0);
    });
    test("should return an empty object as it got an empty string", () => {
        const result = Parser.parseTags("");
        expect(result.size).toEqual(0);
    });
    test("should return an empty object as there is no valid tag", () => {
        const str = "@;;;";
        const result = Parser.parseTags(str);
        expect(result.size).toEqual(0);
    });
    test("should return an object with 2 tags in it", () => {
        const str = "@badges=vip,mod,cheer150000;mod=1;display=TestING;";
        const result = Parser.parseTags(str);
        expect(result.size).toEqual(3);
    });
});
describe("parseType", () => {
    test("should return CODE_376", () => {
        expect(Parser.parseType("376")).toEqual(IRC_TYPES.CODE_376);
    });
    test("should return CAP", () => {
        expect(Parser.parseType("CAP")).toEqual(IRC_TYPES.CAP);
    });
    test("should return JOIN", () => {
        expect(Parser.parseType("JOIN")).toEqual(IRC_TYPES.JOIN);
    });
    test("should return PART", () => {
        expect(Parser.parseType("PART")).toEqual(IRC_TYPES.PART);
    });
    test("should return PING", () => {
        expect(Parser.parseType("PING")).toEqual(IRC_TYPES.PING);
    });
    test("should return PONG", () => {
        expect(Parser.parseType("PONG")).toEqual(IRC_TYPES.PONG);
    });
    test("should return PRIVMSG", () => {
        expect(Parser.parseType("PRIVMSG")).toEqual(IRC_TYPES.PRIVMSG);
    });
    test("should return ROOMSTATE", () => {
        expect(Parser.parseType("ROOMSTATE")).toEqual(IRC_TYPES.ROOMSTATE);
    });
    test("should return USERNOTICE", () => {
        expect(Parser.parseType("USERNOTICE")).toEqual(IRC_TYPES.USERNOTICE);
    });
    test("should return USERSTATE", () => {
        expect(Parser.parseType("USERSTATE")).toEqual(IRC_TYPES.USERSTATE);
    });
    test("should return CLEARCHAT", () => {
        expect(Parser.parseType("CLEARCHAT")).toEqual(IRC_TYPES.CLEARCHAT);
    });
    test("should return CLEARMSG", () => {
        expect(Parser.parseType("CLEARMSG")).toEqual(IRC_TYPES.CLEARMSG);
    });
    test("should return NOTICE", () => {
        expect(Parser.parseType("NOTICE")).toEqual(IRC_TYPES.NOTICE);
    });
    test("should return HOSTTARGET", () => {
        expect(Parser.parseType("HOSTTARGET")).toEqual(IRC_TYPES.HOSTTARGET);
    });
    test("should return UNKNOWN as it is unknown", () => {
        expect(Parser.parseType("TESTTYPE")).toEqual(IRC_TYPES.UNKNOWN);
    });
    test("should return UNKNOWN as it is null", () => {
        expect(Parser.parseType(null)).toEqual(IRC_TYPES.UNKNOWN);
    });
    test("should return UNKNOWN as it is empty", () => {
        expect(Parser.parseType("")).toEqual(IRC_TYPES.UNKNOWN);
    });
});
describe("isPing", () => {
    test("should approve as it is a good PING request", () => {
        expect(Parser.isPing("PING :tmi.twitch.tv")).toBeTruthy();
    });
    test("should reject as it is a PONG request", () => {
        expect(Parser.isPing("PONG :tmi.twitch.tv")).toBeFalsy();
    });
    test("should reject as this is a PRIVMSG", () => {
        expect(Parser.isPing(":tmi.twitch.tv PRIVMSG #channel :message")).toBeFalsy();
    });
});
describe("isPong", () => {
    test("should approve as it is a good PONG request", () => {
        expect(Parser.isPong("PONG :tmi.twitch.tv")).toBeTruthy();
    });
    test("should reject as it is a PING request", () => {
        expect(Parser.isPong("PING :tmi.twitch.tv")).toBeFalsy();
    });
    test("should reject as this is a PRIVMSG", () => {
        expect(Parser.isPong(":tmi.twitch.tv PRIVMSG #channel :message")).toBeFalsy();
    });
});
describe("hasColonChar", () => {
    test("should return false as the message starts with @", () => {
        expect(Parser.hasColonChar("@badges=vip,cheer150000;mod=0;display=Test")).toBeFalsy();
    });
    test("should return true as it starts with a server info", () => {
        expect(Parser.hasColonChar(":tmi@tmi.twitch.tv PRIVMSG #test :hello tester")).toBeTruthy();
    });
    test("should return false as it is a PING", () => {
        expect(Parser.hasColonChar("PING :tmi.twitch.tv")).toBeFalsy();
    });
});
describe("parsePrefix", () => {
    test("should fail to parse as this is not a good format", async () => {
        const prefixes = [
            Parser.parsePrefix("@badges=vip,cheer150000;mod=0;display=Test"),
            Parser.parsePrefix("PING :tmi.twitch.tv"),
        ];
        expect.assertions(4 * prefixes.length);
        prefixes.forEach((prefix) => {
            expect(prefix.size).toEqual(0);
            expect(prefix.has("nick")).toBeFalsy();
            expect(prefix.has("host")).toBeFalsy();
            expect(prefix.has("user")).toBeFalsy();
        });
    });
    test("should parse correctly but only obtain a nick/servername as it misses structure", async () => {
        const prefixes = [
            Parser.parsePrefix(":jtv PRIVMSG testuser :hello tester"),
            Parser.parsePrefix(":user.tmi.twitch.tv JOIN #test"),
        ];
        expect.assertions(4 * prefixes.length);
        prefixes.forEach((prefix) => {
            expect(prefix.size).toEqual(1);
            expect(prefix.has("host")).toBeTruthy();
            expect(prefix.has("nick")).toBeFalsy();
            expect(prefix.has("user")).toBeFalsy();
        });
    });
    test("should fully succeed the parsing of the prefix", async () => {
        const prefixes = [
            Parser.parsePrefix(":usernick!username@tmi.twitch.tv PRIVMSG #test :hello tester"),
            Parser.parsePrefix(":ronni!ronni@ronni.tmi.twitch.tv JOIN #dallas"),
        ];
        expect.assertions(4 * prefixes.length);
        prefixes.forEach((prefix) => {
            expect(prefix.size).toEqual(3);
            expect(prefix.has("nick")).toBeTruthy();
            expect(prefix.has("host")).toBeTruthy();
            expect(prefix.has("user")).toBeTruthy();
        });
    });
    test("should return an empty map if the given prefix is an empty string", async () => {
        const prefix = Parser.parsePrefix("");
        expect(prefix.size).toEqual(0);
        expect(prefix.has("nick")).toBeFalsy();
        expect(prefix.has("host")).toBeFalsy();
        expect(prefix.has("user")).toBeFalsy();
    });
});
describe("getNext", () => {
    test("should return undefined if there is no space left", async () => {
        expect(Parser.getNext("username")).toBeUndefined();
    });
    test("should return the next body", async () => {
        expect(Parser.getNext("another message")).toEqual("message");
    });
});
describe("hasNext", () => {
    test("should return false if there is no space left", async () => {
        expect(Parser.hasNext("username")).toBeFalsy();
    });
    test("should return true if there is spaces left", async () => {
        expect(Parser.hasNext("another message")).toBeTruthy();
    });
});
describe("parseContent", () => {
    test("should return the message", async () => {
        expect(Parser.parseContent(":another beautiful day! Hi, streamer :)")).not.toBeNull();
    });
    test("should return undefined, as there is no message", async () => {
        expect(Parser.parseContent(":")).toBeUndefined();
    });
    test("should return undefined as there is no message identifier (':')", async () => {
        expect(Parser.parseContent("the quick brown fox jumps over the lazy dog")).toBeUndefined();
    });
});
describe("getFirst", () => {
    test("should return undefined as the message is empty", async () => {
        expect(Parser.getFirst("")).toBeUndefined();
    });
    test("should return the same string as there is no space left", async () => {
        const message = "hello.";
        expect(Parser.getFirst(message)).toEqual(message);
    });
    test("should return the first part of the message", async () => {
        expect(Parser.getFirst("another message")).toEqual("another");
    });
});
describe("parseMessage", () => {
    test("should return undefined as there is no message", () => {
        expect(Parser.parseMessage("")).toBeUndefined();
    });
    test("should be of type PING", () => {
        const result = Parser.parseMessage("PING :tmi.twitch.tv");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PING);
        expect(result.content).toEqual("tmi.twitch.tv");
    });
    test("should be of type PONG", () => {
        const result = Parser.parseMessage("PONG :tmi.twitch.tv");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PONG);
        expect(result.content).toEqual("tmi.twitch.tv");
    });
    test("should be of type CODE_376", () => {
        const result = Parser.parseMessage(":tmi.twitch.tv 376 demouser :>");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CODE_376);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("demouser");
        expect(result.content).toEqual(">");
    });
    test("should be of type PRIVMSG, no tags, just a server prefix host", () => {
        const result = Parser.parseMessage(":jtv PRIVMSG #demouser :Host ended");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("jtv");
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.content).toEqual("Host ended");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#demouser");
    });
    test("should be of type PRIVMSG, no tags, has full prefix", () => {
        const msg = ":demouser1!demouser2@demouser3.tmi.twitch.tv PRIVMSG #demouser :GOOD DAY SIR!";
        const result = Parser.parseMessage(msg);
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("demouser3.tmi.twitch.tv");
        expect(result.prefix.get("user")).toEqual("demouser2");
        expect(result.prefix.get("nick")).toEqual("demouser1");
        expect(result.content).toEqual("GOOD DAY SIR!");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#demouser");
    });
    test("should be of type PRIVMSG, has tags, has prefixes", () => {
        const tags = "@mod=1;display=DemoUser";
        const prefix = ":demouser1!demouser2@demouser3.tmi.twitch.tv";
        const content = "GOOD DAY SIR!";
        const msg = `${tags} ${prefix} PRIVMSG #demouser :${content}`;
        const result = Parser.parseMessage(msg);
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.tags.size).toEqual(2);
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.has("display")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("1");
        expect(result.tags.get("display")).toEqual("DemoUser");
        expect(result.prefix.get("host")).toEqual("demouser3.tmi.twitch.tv");
        expect(result.prefix.get("user")).toEqual("demouser2");
        expect(result.prefix.get("nick")).toEqual("demouser1");
        expect(result.content).toEqual("GOOD DAY SIR!");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#demouser");
    });
    test("should be of type JOIN", () => {
        const result = Parser.parseMessage(":<user>!<user>@<user>.tmi.twitch.tv JOIN #<channel>");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.JOIN);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("<user>.tmi.twitch.tv");
        expect(result.prefix.get("user")).toEqual("<user>");
        expect(result.prefix.get("nick")).toEqual("<user>");
        expect(result.content).toBeNull();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
    });
    test("should ignore JOIN acknowledgement (CODE_353)", () => {
        const result = Parser.parseMessage(":<user>.tmi.twitch.tv 353 <user> = #<channel> :<user>");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.UNKNOWN);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("<user>.tmi.twitch.tv");
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.content).toEqual("<user>");
        expect(result.params.length).toEqual(3);
        expect(result.params[0]).toEqual("<user>");
        expect(result.params[1]).toEqual("=");
        expect(result.params[2]).toEqual("#<channel>");
    });
    test("should ignore NAMES acknowledgement (CODE_366)", () => {
        const result = Parser.parseMessage(":<user>.tmi.twitch.tv 366 <user> #<channel> :End of /NAMES list");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.UNKNOWN);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("<user>.tmi.twitch.tv");
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.content).toEqual("End of /NAMES list");
        expect(result.params.length).toEqual(2);
        expect(result.params[0]).toEqual("<user>");
        expect(result.params[1]).toEqual("#<channel>");
    });
    test("should be of type PART", () => {
        const result = Parser.parseMessage(":<user>!<user>@<user>.tmi.twitch.tv PART #<channel>");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PART);
        expect(result.tags).toBeNull();
        expect(result.prefix.get("host")).toEqual("<user>.tmi.twitch.tv");
        expect(result.prefix.get("user")).toEqual("<user>");
        expect(result.prefix.get("nick")).toEqual("<user>");
        expect(result.content).toBeNull();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
    });
    test("should be of type PRIVMSG", () => {
        const message = ":<user>!<user>@<user>.tmi.twitch.tv PRIVMSG #<channel> :This is a sample message";
        const result = Parser.parseMessage(message);
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.content).toEqual("This is a sample message");
        expect(result.prefix.get("nick")).toEqual("<user>");
        expect(result.prefix.get("user")).toEqual("<user>");
        expect(result.prefix.get("host")).toEqual("<user>.tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
    });
    test("should be a CAP ACK of membership", () => {
        const message = ":tmi.twitch.tv CAP * ACK :twitch.tv/membership";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CAP);
        expect(result.params.length).toEqual(2);
        expect(result.params[0]).toEqual("*");
        expect(result.params[1]).toEqual("ACK");
        expect(result.content).toEqual("twitch.tv/membership");
        expect(result.tags).toBeNull();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
    });
    test("should be a CAP NAK of tags", () => {
        const message = ":tmi.twitch.tv CAP * NAK :twitch.tv/tags";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CAP);
        expect(result.params.length).toEqual(2);
        expect(result.params[0]).toEqual("*");
        expect(result.params[1]).toEqual("NAK");
        expect(result.content).toEqual("twitch.tv/tags");
        expect(result.tags).toBeNull();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
    });
    test("should be a CLEARCHAT (purge of a user)", () => {
        const message = ":tmi.twitch.tv CLEARCHAT #<channel> :<user>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARCHAT);
        expect(result.tags).toBeNull();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.content).toEqual("<user>");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
    });
    test("should be a CLEARCHAT (whole chat cleared)", () => {
        const message = ":tmi.twitch.tv CLEARCHAT #dallas";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARCHAT);
        expect(result.tags).toBeNull();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.content).toBeNull();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
    });
    test("should be a CLEARMSG", () => {
        const message = "@login=<login>;target-msg-id=<target-msg-id> :tmi.twitch.tv CLEARMSG #<channel> :<message>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARMSG);
        expect(result.tags.size).toEqual(2);
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("<login>");
        expect(result.tags.has("target-msg-id")).toBeTruthy();
        expect(result.tags.get("target-msg-id")).toEqual("<target-msg-id>");
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toEqual("<message>");
    });
    test("should be a CLEARMSG (example data)", () => {
        const message = "@login=ronni;target-msg-id=abc-123-def :tmi.twitch.tv CLEARMSG #dallas :HeyGuys";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARMSG);
        expect(result.tags.size).toEqual(2);
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("ronni");
        expect(result.tags.has("target-msg-id")).toBeTruthy();
        expect(result.tags.get("target-msg-id")).toEqual("abc-123-def");
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.content).toEqual("HeyGuys");
    });
    test("should be a HOSTTARGET (we are hosting)", () => {
        const message = ":tmi.twitch.tv HOSTTARGET #hosting_channel <channel> [<number-of-viewers>]";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.HOSTTARGET);
        expect(result.tags).toBeNull();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(3);
        expect(result.params[0]).toEqual("#hosting_channel");
        expect(result.params[1]).toEqual("<channel>");
        expect(result.params[2]).toEqual("[<number-of-viewers>]");
        expect(result.content).toBeNull();
    });
    test("should be detected as a NOTICE (generic example)", () => {
        const message = "@msg-id=<msg-id> :tmi.twitch.tv NOTICE #<channel> :<message>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.NOTICE);
        expect(result.tags.size).toEqual(1);
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toEqual("<message>");
    });
    test("should detect a ROOMSTATE", () => {
        const message = ":tmi.twitch.tv ROOMSTATE #<channel>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.ROOMSTATE);
        expect(result.tags).toBeNull();
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toBeNull();
    });
    test("should detect a USERNOTICE", () => {
        const message = ":tmi.twitch.tv USERNOTICE #<channel> :message";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.tags).toBeNull();
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toEqual("message");

    });
    test("should detect a base USERSTATE", () => {
        const message = ":tmi.twitch.tv USERSTATE #<channel>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERSTATE);
        expect(result.tags).toBeNull();
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toBeNull();
    });
    test("should ignore a undefined param found by the parseParam", () => {
        const message = ":tmi.twitch.tv USERSTATE ignored #<channel>";
        const parseParamFn = jest.spyOn(Parser, "parseParam");
        parseParamFn
        .mockImplementationOnce(() => undefined)
        .mockImplementationOnce(() => "#<channel>");
        const result = Parser.parseMessage(message);
        parseParamFn.mockRestore();

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERSTATE);
        expect(result.tags).toBeNull();
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toBeNull();
    });
    test("should detect a timeout", () => {
        const message = "@ban-duration=<ban-duration> :tmi.twitch.tv CLEARCHAT #<channel> :<user>";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARCHAT);
        expect(result.tags.size).toEqual(1);
        expect(result.tags.has("ban-duration")).toBeTruthy();
        expect(result.tags.get("ban-duration")).toEqual("<ban-duration>");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#<channel>");
        expect(result.content).toEqual("<user>");
    });
    test("should report a CLEARCHAT on ronni from #dallas channel", () => {
        const message = ":tmi.twitch.tv CLEARCHAT #dallas :ronni";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARCHAT);
        expect(result.tags).toBeNull();
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.content).toEqual("ronni");
    });
    test("should return a CLEARMSG for a given message", () => {
        const message = "@login=ronni;target-msg-id=abc-123-def :tmi.twitch.tv CLEARMSG #dallas :HeyGuys";
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.CLEARMSG);
        expect(result.tags.size).toEqual(2);
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("ronni");
        expect(result.tags.has("target-msg-id")).toBeTruthy();
        expect(result.tags.get("target-msg-id")).toEqual("abc-123-def");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.content).toEqual("HeyGuys");
    });
    test("should return the GLOBALUSERSTATE of the connected bot user", () => {
        let tags = "@badges=staff/1;color=#0D4200;display-name=dallas;";
        tags += "emote-sets=0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239;";
        tags += "turbo=0;user-id=1337;user-type=admin";
        const message = `${tags} :tmi.twitch.tv GLOBALUSERSTATE`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.GLOBALUSERSTATE);
        expect(result.tags.size).toEqual(7);
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("staff/1");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("#0D4200");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("dallas");
        expect(result.tags.has("emote-sets")).toBeTruthy();
        expect(result.tags.get("emote-sets")).toEqual("0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("0");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("1337");
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("admin");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.params.length).toEqual(0);
        expect(result.content).toBeNull();
    });
    test("should be a full-featured PRIVMSG with tags", () => {
        let tags = "@badges=global_mod/1,turbo/1;color=#0D4200;display-name=dallas;";
        tags += "emotes=25:0-4,12-16/1902:6-10;id=b34ccfc7-4977-403a-8a94-33c6bac34fb8;";
        tags += "mod=0;room-id=1337;subscriber=0;tmi-sent-ts=1507246572675;";
        tags += "turbo=1;user-id=1337;user-type=global_mod";
        const message = `${tags} :ronni!ronni@ronni.tmi.twitch.tv PRIVMSG #dallas :Kappa Keepo Kappa`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.tags.size).toEqual(12);
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("global_mod/1,turbo/1");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("#0D4200");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("dallas");
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.get("emotes")).toEqual("25:0-4,12-16/1902:6-10");
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.get("id")).toEqual("b34ccfc7-4977-403a-8a94-33c6bac34fb8");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("1337");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("0");
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.get("tmi-sent-ts")).toEqual("1507246572675");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("1");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("1337");
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("global_mod");
        expect(result.prefix.size).toEqual(3);
        expect(result.prefix.has("nick")).toBeTruthy();
        expect(result.prefix.get("nick")).toEqual("ronni");
        expect(result.prefix.has("user")).toBeTruthy();
        expect(result.prefix.get("user")).toEqual("ronni");
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("ronni.tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.content).toEqual("Kappa Keepo Kappa");
    });
    test("should have a bits tag", () => {
        let tags = "@badges=staff/1,bits/1000;bits=100;color=;display-name=dallas;";
        tags += "emotes=;id=b34ccfc7-4977-403a-8a94-33c6bac34fb8;mod=0;room-id=1337;subscriber=0;";
        tags += "tmi-sent-ts=1507246572675;turbo=1;user-id=1337;user-type=staff";
        const message = `${tags} :ronni!ronni@ronni.tmi.twitch.tv PRIVMSG #dallas :cheer100`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.PRIVMSG);
        expect(result.tags.size).toEqual(13);
        expect(result.tags.get("badges")).toEqual("staff/1,bits/1000");
        expect(result.tags.get("bits")).toEqual("100");
        expect(result.tags.get("color")).toEqual("");
        expect(result.tags.get("display-name")).toEqual("dallas");
        expect(result.tags.get("emotes")).toEqual("");
        expect(result.tags.get("id")).toEqual("b34ccfc7-4977-403a-8a94-33c6bac34fb8");
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.get("room-id")).toEqual("1337");
        expect(result.tags.get("subscriber")).toEqual("0");
        expect(result.tags.get("tmi-sent-ts")).toEqual("1507246572675");
        expect(result.tags.get("turbo")).toEqual("1");
        expect(result.tags.get("user-id")).toEqual("1337");
        expect(result.tags.get("user-type")).toEqual("staff");
        expect(result.prefix.size).toEqual(3);
        expect(result.prefix.has("nick")).toBeTruthy();
        expect(result.prefix.get("nick")).toEqual("ronni");
        expect(result.prefix.has("user")).toBeTruthy();
        expect(result.prefix.get("user")).toEqual("ronni");
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("ronni.tmi.twitch.tv");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.content).toEqual("cheer100");
    });
    test("should obtain a ROOMSTATE (status of the current channel)", () => {
        const tags = "@broadcaster-lang=en;emote-only=0;followers-only=0;r9k=0;slow=0;subs-only=0";
        const message = `${tags} :tmi.twitch.tv ROOMSTATE #dallas`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.ROOMSTATE);
        expect(result.tags.has("broadcaster-lang")).toBeTruthy();
        expect(result.tags.get("broadcaster-lang")).toEqual("en");
        expect(result.tags.has("emote-only")).toBeTruthy();
        expect(result.tags.get("emote-only")).toEqual("0");
        expect(result.tags.has("followers-only")).toBeTruthy();
        expect(result.tags.get("followers-only")).toEqual("0");
        expect(result.tags.has("r9k")).toBeTruthy();
        expect(result.tags.get("r9k")).toEqual("0");
        expect(result.tags.has("slow")).toBeTruthy();
        expect(result.tags.get("slow")).toEqual("0");
        expect(result.tags.has("subs-only")).toBeTruthy();
        expect(result.tags.get("subs-only")).toEqual("0");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
    });
    test("should detect a ROOMSTATE (reporting a slow mode of 10s)", () => {
        const result = Parser.parseMessage("@slow=10 :tmi.twitch.tv ROOMSTATE #dallas");
        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.ROOMSTATE);
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
    });
    test("should detect a sub/resub", () => {
        let tags = "@badges=staff/1,broadcaster/1,turbo/1;color=#008000;display-name=ronni;emotes=;";
        tags += "id=db25007f-7a18-43eb-9379-80131e44d633;login=ronni;mod=0;msg-id=resub;";
        tags += "msg-param-months=6;msg-param-sub-plan=Prime;msg-param-sub-plan-name=Prime;";
        tags += "room-id=1337;subscriber=1;system-msg=ronni\shas\ssubscribed\sfor\s6\smonths!;";
        tags += "tmi-sent-ts=1507246572675;turbo=1;user-id=1337;user-type=staff";
        const message = `${tags} :tmi.twitch.tv USERNOTICE #dallas :Great stream -- keep it up!`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.content).toEqual("Great stream -- keep it up!");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.tags.size).toEqual(18);
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("staff/1,broadcaster/1,turbo/1");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("#008000");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("ronni");
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.get("emotes")).toEqual("");
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.get("id")).toEqual("db25007f-7a18-43eb-9379-80131e44d633");
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("ronni");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.get("msg-id")).toEqual("resub");
        expect(result.tags.has("msg-param-months")).toBeTruthy();
        expect(result.tags.get("msg-param-months")).toEqual("6");
        expect(result.tags.has("msg-param-sub-plan")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan")).toEqual("Prime");
        expect(result.tags.has("msg-param-sub-plan-name")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan-name")).toEqual("Prime");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("1337");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("1");
        expect(result.tags.has("system-msg")).toBeTruthy();
        expect(result.tags.get("system-msg")).toEqual("ronni\shas\ssubscribed\sfor\s6\smonths!");
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.get("tmi-sent-ts")).toEqual("1507246572675");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("1");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("1337");
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("staff");
    });
    test("should detect a sub gift", () => {
        let tags = "@badges=staff/1,premium/1;color=#0000FF;display-name=TWW2;emotes=;";
        tags += "id=e9176cd8-5e22-4684-ad40-ce53c2561c5e;login=tww2;mod=0;msg-id=subgift;";
        tags += "msg-param-months=1;msg-param-recipient-display-name=Mr_Woodchuck;";
        tags += "msg-param-recipient-id=89614178;msg-param-recipient-name=mr_woodchuck;";
        tags += "msg-param-sub-plan-name=House\sof\sNyoro~n;msg-param-sub-plan=1000;room-id=19571752;";
        tags += "subscriber=0;system-msg=TWW2\sgifted\sa\sTier\s1\ssub\sto\sMr_Woodchuck!;";
        tags += "tmi-sent-ts=1521159445153;turbo=0;user-id=13405587;user-type=staff";
        const message = `${tags} :tmi.twitch.tv USERNOTICE #forstycup`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.content).toBeNull();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#forstycup");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.tags.size).toEqual(21);
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("staff");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("13405587");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("0");
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.get("tmi-sent-ts")).toEqual("1521159445153");
        expect(result.tags.has("system-msg")).toBeTruthy();
        expect(result.tags.get("system-msg")).toEqual("TWW2\sgifted\sa\sTier\s1\ssub\sto\sMr_Woodchuck!");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("0");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("19571752");
        expect(result.tags.has("msg-param-sub-plan")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan")).toEqual("1000");
        expect(result.tags.has("msg-param-sub-plan-name")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan-name")).toEqual("House\sof\sNyoro~n");
        expect(result.tags.has("msg-param-recipient-name")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-name")).toEqual("mr_woodchuck");
        expect(result.tags.has("msg-param-recipient-id")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-id")).toEqual("89614178");
        expect(result.tags.has("msg-param-recipient-display-name")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-display-name")).toEqual("Mr_Woodchuck");
        expect(result.tags.has("msg-param-months")).toBeTruthy();
        expect(result.tags.get("msg-param-months")).toEqual("1");
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.get("msg-id")).toEqual("subgift");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("tww2");
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.get("id")).toEqual("e9176cd8-5e22-4684-ad40-ce53c2561c5e");
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.get("emotes")).toEqual("");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("TWW2");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("#0000FF");
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("staff/1,premium/1");
    });
    test("should detect an anonymous sub gift", () => {
        let tags = "@badges=broadcaster/1,subscriber/6;color=;display-name=qa_subs_partner;";
        tags += "emotes=;flags=;id=b1818e3c-0005-490f-ad0a-804957ddd760;login=qa_subs_partner;mod=0;";
        tags += "msg-id=anonsubgift;msg-param-months=3;msg-param-recipient-display-name=TenureCalculator;";
        tags += "msg-param-recipient-id=135054130;msg-param-recipient-user-name=tenurecalculator;";
        tags += "msg-param-sub-plan-name=t111;msg-param-sub-plan=1000;room-id=196450059;";
        tags += "subscriber=1;system-msg=An\sanonymous\suser\sgifted\sa\sTier\s1\ssub\sto\sTenureCalculator!\s;";
        tags += "tmi-sent-ts=1542063432068;turbo=0;user-id=196450059;user-type=";
        const message = `${tags} :tmi.twitch.tv USERNOTICE #qa_subs_partner`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.content).toBeNull();
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#qa_subs_partner");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.tags.size).toEqual(22);
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("196450059");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("0");
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.get("tmi-sent-ts")).toEqual("1542063432068");
        expect(result.tags.has("system-msg")).toBeTruthy();
        expect(result.tags.get("system-msg"))
            .toEqual("An\sanonymous\suser\sgifted\sa\sTier\s1\ssub\sto\sTenureCalculator!\s");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("1");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("196450059");
        expect(result.tags.has("msg-param-sub-plan")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan")).toEqual("1000");
        expect(result.tags.has("msg-param-sub-plan-name")).toBeTruthy();
        expect(result.tags.get("msg-param-sub-plan-name")).toEqual("t111");
        expect(result.tags.has("msg-param-recipient-user-name")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-user-name")).toEqual("tenurecalculator");
        expect(result.tags.has("msg-param-recipient-id")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-id")).toEqual("135054130");
        expect(result.tags.has("msg-param-recipient-display-name")).toBeTruthy();
        expect(result.tags.get("msg-param-recipient-display-name")).toEqual("TenureCalculator");
        expect(result.tags.has("msg-param-months")).toBeTruthy();
        expect(result.tags.get("msg-param-months")).toEqual("3");
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.get("msg-id")).toEqual("anonsubgift");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("qa_subs_partner");
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.get("id")).toEqual("b1818e3c-0005-490f-ad0a-804957ddd760");
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.get("emotes")).toEqual("");
        expect(result.tags.has("flags")).toBeTruthy();
        expect(result.tags.get("flags")).toEqual("");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("qa_subs_partner");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("");
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("broadcaster/1,subscriber/6");
    });
    test("should detect a raid from another channel", () => {
        let tags = "@badges=turbo/1;color=#9ACD32;display-name=TestChannel;emotes=;";
        tags += "id=3d830f12-795c-447d-af3c-ea05e40fbddb;login=testchannel;mod=0;";
        tags += "msg-id=raid;msg-param-displayName=TestChannel;msg-param-login=testchannel;";
        tags += "msg-param-viewerCount=15;room-id=56379257;subscriber=0;";
        tags += "system-msg=15\sraiders\sfrom\sTestChannel\shave\sjoined!;";
        tags += "tmi-sent-ts=1507246572675;turbo=1;user-id=123456;user-type=";
        const message = `${tags} :tmi.twitch.tv USERNOTICE #othertestchannel`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#othertestchannel");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.tags.size).toEqual(18);
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.has("system-msg")).toBeTruthy();
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.has("msg-param-viewerCount")).toBeTruthy();
        expect(result.tags.has("msg-param-login")).toBeTruthy();
        expect(result.tags.has("msg-param-displayName")).toBeTruthy();
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.has("color")).toBeTruthy();
    });
    test("should detect a new-comer ritual in the tags", () => {
        let tags = "@badges=;color=;display-name=SevenTest1;emotes=30259:0-6;";
        tags += "id=37feed0f-b9c7-4c3a-b475-21c6c6d21c3d;login=seventest1;mod=0;";
        tags += "msg-id=ritual;msg-param-ritual-name=new_chatter;room-id=6316121;";
        tags += "subscriber=0;system-msg=Seventoes\sis\snew\shere!;tmi-sent-ts=1508363903826;";
        tags += "turbo=0;user-id=131260580;user-type=";
        const message = `${tags} :tmi.twitch.tv USERNOTICE #seventoes :HeyGuys`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERNOTICE);
        expect(result.content).toEqual("HeyGuys");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#seventoes");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.tags.size).toEqual(16);
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("SevenTest1");
        expect(result.tags.has("emotes")).toBeTruthy();
        expect(result.tags.get("emotes")).toEqual("30259:0-6");
        expect(result.tags.has("id")).toBeTruthy();
        expect(result.tags.get("id")).toEqual("37feed0f-b9c7-4c3a-b475-21c6c6d21c3d");
        expect(result.tags.has("login")).toBeTruthy();
        expect(result.tags.get("login")).toEqual("seventest1");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("0");
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.get("msg-id")).toEqual("ritual");
        expect(result.tags.has("msg-param-ritual-name")).toBeTruthy();
        expect(result.tags.get("msg-param-ritual-name")).toEqual("new_chatter");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("6316121");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("0");
        expect(result.tags.has("system-msg")).toBeTruthy();
        expect(result.tags.get("system-msg")).toEqual("Seventoes\sis\snew\shere!");
        expect(result.tags.has("tmi-sent-ts")).toBeTruthy();
        expect(result.tags.get("tmi-sent-ts")).toEqual("1508363903826");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("0");
        expect(result.tags.has("user-id")).toBeTruthy();
        expect(result.tags.get("user-id")).toEqual("131260580");
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("");
    });
    test("should detect a userstate for the given user in a given channel", () => {
        let tags = "@badges=staff/1;color=#0D4200;display-name=ronni;";
        tags += "emote-sets=0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239;";
        tags += "mod=1;subscriber=1;turbo=1;user-type=staff";
        const message = `${tags} :tmi.twitch.tv USERSTATE #dallas`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.USERSTATE);
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#dallas");
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.tags.size).toEqual(8);
        expect(result.tags.has("badges")).toBeTruthy();
        expect(result.tags.get("badges")).toEqual("staff/1");
        expect(result.tags.has("color")).toBeTruthy();
        expect(result.tags.get("color")).toEqual("#0D4200");
        expect(result.tags.has("display-name")).toBeTruthy();
        expect(result.tags.get("display-name")).toEqual("ronni");
        expect(result.tags.has("emote-sets")).toBeTruthy();
        expect(result.tags.get("emote-sets")).toEqual("0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239");
        expect(result.tags.has("mod")).toBeTruthy();
        expect(result.tags.get("mod")).toEqual("1");
        expect(result.tags.has("subscriber")).toBeTruthy();
        expect(result.tags.get("subscriber")).toEqual("1");
        expect(result.tags.has("turbo")).toBeTruthy();
        expect(result.tags.get("turbo")).toEqual("1");
        expect(result.tags.has("user-type")).toBeTruthy();
        expect(result.tags.get("user-type")).toEqual("staff");
        expect(result.content).toBeNull();
    });
    test("should detect a JOIN notice of a chatroom", () => {
        const params = "#chatrooms:44322889:04e762ec-ce8f-4cbc-b6a3-ffc871ab53da";
        const message = `:ronni!ronni@ronni.tmi.twitch.tv JOIN ${params}`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.JOIN);
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual(params);
        expect(result.prefix.size).toEqual(3);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("ronni.tmi.twitch.tv");
        expect(result.prefix.has("user")).toBeTruthy();
        expect(result.prefix.get("user")).toEqual("ronni");
        expect(result.prefix.has("nick")).toBeTruthy();
        expect(result.prefix.get("nick")).toEqual("ronni");
        expect(result.tags).toBeNull();
    });
    test("should detect a NOTICE about slow-mode going off from a chatroom", () => {
        const params = "#chatrooms:44322889:04e762ec-ce8f-4cbc-b6a3-ffc871ab53da";
        const content = ":This room is no longer in slow mode.";
        const message = `@msg-id=slow_off :tmi.twitch.tv NOTICE ${params} ${content}`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.NOTICE);
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.tags.size).toEqual(1);
        expect(result.tags.has("msg-id")).toBeTruthy();
        expect(result.tags.get("msg-id")).toEqual("slow_off");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#chatrooms:44322889:04e762ec-ce8f-4cbc-b6a3-ffc871ab53da");
        expect(result.content).toEqual("This room is no longer in slow mode.");
    });
    test("should detect a roomstate for a chatroom", () => {
        const tags = "@emote-only=<emote-only>;room-id=<channel-ID>;r9k=<r9k>;slow=<slow>;";
        const message = `${tags} :tmi.twitch.tv ROOMSTATE #chatrooms:<channel-ID>:<room-UUID>:<message>`;
        const result = Parser.parseMessage(message);

        if (!result) {
            fail("result should be defined in this case");
            return;
        }
        expect(result.type).toEqual(IRC_TYPES.ROOMSTATE);
        expect(result.prefix.size).toEqual(1);
        expect(result.prefix.has("host")).toBeTruthy();
        expect(result.prefix.get("host")).toEqual("tmi.twitch.tv");
        expect(result.prefix.has("nick")).toBeFalsy();
        expect(result.prefix.has("user")).toBeFalsy();
        expect(result.tags.size).toEqual(4);
        expect(result.tags.has("emote-only")).toBeTruthy();
        expect(result.tags.get("emote-only")).toEqual("<emote-only>");
        expect(result.tags.has("room-id")).toBeTruthy();
        expect(result.tags.get("room-id")).toEqual("<channel-ID>");
        expect(result.tags.has("r9k")).toBeTruthy();
        expect(result.tags.get("r9k")).toEqual("<r9k>");
        expect(result.tags.has("slow")).toBeTruthy();
        expect(result.tags.get("slow")).toEqual("<slow>");
        expect(result.params.length).toEqual(1);
        expect(result.params[0]).toEqual("#chatrooms:<channel-ID>:<room-UUID>:<message>");
    });
});
