// Native modules & NPM
import { EventEmitter } from "events";
// Custom modules
import { Parser } from "./parser";
import { IParsedMessage } from "./message-factory";
import { IRC_TYPES } from "./message-types";
import WebSocket from "ws";

export interface IClientOptions {
    hostname?: string;
    reconnect?: boolean;
    nickname?: string;
    password?: string;
    capabilities?: string[];
    heartbeat?: number;
}
export interface IClientSettings {
    hostname: string;
    reconnect: boolean;
    nickname: string;
    password?: string;
    capabilities: string[];
    heartbeat: number;
}

export interface Client {
    // events the Client emits
    on(event: "reconnect"): void;
    on(event: "connected"): void;
    on(event: "closed"): void;
    on(event: "send", message: string): void;
    // events the user can trigger
    emit(event: "start"): boolean;
    emit(event: "stop"): boolean;
    emit(event: "send", message: string): boolean;
    // standard implementations
    on(event: string | symbol, listener: (...args: any[]) => void): this;
    emit(event: string | symbol, ...args: any[]): boolean;
}

export class Client extends EventEmitter {
    public enabled: boolean;
    public authenticated: boolean;
    public config: IClientSettings;

    public pinger: NodeJS.Timeout | undefined;
    public timeout: NodeJS.Timeout | undefined;
    public socket: WebSocket | undefined;

    constructor(options: IClientOptions) {
        super();
        // set configuration for the client
        this.config = Object.assign({}, {
            capabilities: [],
            hostname: "wss://irc-ws.chat.twitch.tv:443",
            nickname: `justinfan${Math.floor(Math.random()*100000)}`,
            password: undefined,
            reconnect: true,
            heartbeat: 10000,
        }, options);
        this.enabled = false;
        this.authenticated = false;
        this.on("start", () => this.onStart());
        this.on("stop", () => this.onStop());
        this.on("send", (message) => this.send(message));
    }

    public onStart() {
        if (this.enabled) {
            return;
        }
        this.bindEvents();
        this.initializeSocket();
    }

    public async onStop() {
        if (!this.enabled) {
            return;
        }
        await this.terminateSocket();
        this.enabled = false;
        this.authenticated = false;
        this.emit("terminated");
    }

    public heartbeat() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.socket?.close();
        }, (this.config.heartbeat || 10000) * 1.5);
        this.timeout.unref();
    }
    public isConnected (): boolean {
        return this.socket?.readyState === WebSocket.OPEN;
    }
    public onConnectionClosed(code: number, reason: string) {
        this.emit("closed");
    }
    public onConnectionError(err: Error) {
        this.emit("error");
    }
    public onConnectionOpen() {
        if (this.config.capabilities && this.config.capabilities.length) {
            this.emit("send", `CAP REQ :${this.config.capabilities.join(" ")}`);
        } else {
            this.sendConnectionInformations();
        }
    }
    public onConnectionMessage(message: WebSocket.Data) {
        this.heartbeat();
        this.handleRawMessage(message);
    }
    public initializeSocket() {
        if (this.isConnected()) {
            return;
        }
        this.enabled = true;
        this.socket = new WebSocket(this.config.hostname, {
            timeout: 5000,
        });
        this.socket.on("close", (code: number, reason: string) => this.onConnectionClosed(code, reason));
        this.socket.on("open", () => this.onConnectionOpen());
        this.socket.on("error", (error) => this.onConnectionError(error));
        this.socket.on("message", (message: WebSocket.Data) => this.onConnectionMessage(message));
        this.emit("initialized");
    }
    public async terminateSocket(): Promise<void> {
        this.socket?.removeAllListeners();
        this.socket?.close();
        delete this.socket;
    }

    public send(message: string): void {
        if (!this.isConnected()) {
            return;
        }
        this.socket?.send(message);
    }
    public bindEvents() {
        this.on("failed", (err?: Error) => {
            process.exit(1);
        });
        this.on("closed", async () => {
            await this.onStop();
            if (this.pinger) {
                clearInterval(this.pinger);
            }
            if (this.config.reconnect) {
                this.emit("reconnect");
                this.initializeSocket();
            }
        });
        this.on("ready", () => {
            this.pinger = setInterval(() => this.ping(), this.config.heartbeat || 10000);
            this.pinger.unref();
        });
    }
    public handleRawMessage(rawMessage: WebSocket.Data) {
        const rawMessageString = rawMessage.toString('utf8');
        const messages = rawMessageString.split("\r\n");
        messages.forEach((message) => {
            if (message.length > 0) {
                const parsedMessage = Parser.parseMessage(message);
                if (parsedMessage) {
                    this.handleParsedMessage(parsedMessage);
                }
            }
        });
    }
    public handleParsedMessage(message: IParsedMessage) {
        if (message.type === IRC_TYPES.CAP) {
            // Capabilities have been taken into account, authenticating
            this.sendConnectionInformations();
        } else if (message.type === IRC_TYPES.CODE_376) {
            // ready to join channels, etc
            this.authenticated = true;
            this.emit("ready", null);
        } else if (message.type === IRC_TYPES.PING) {
            if (!message || !message.content) {
                return;
            }
            this.pong(message.content);
        } else {
            // whatever it is, that is not any of those, we send it!
            if (message.type === IRC_TYPES.PRIVMSG) {
            }
            this.emit("message", message);
        }
    }
    public sendConnectionInformations() {
        if (this.authenticated) {
            return;
        }
        if (this.config.password) {
            this.emit("send", `PASS ${this.config.password}`);
        }
        this.emit("send", `NICK ${this.config.nickname}`);
    }
    public ping() {
        if (!this.isConnected()) {
            return;
        }
        this.emit("send", "PING");
    }
    public pong(pingOrigin: string) {
        if (!this.isConnected()) {
            return;
        }
        this.emit("send", `PONG :${pingOrigin}`);
    }
}

export default { Client };
