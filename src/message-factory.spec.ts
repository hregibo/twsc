import { IrcMessageFactory } from "./message-factory";
import { IRC_TYPES } from "./message-types";

describe("IrcMessageFactory", () => {
    test("should create an empty message", () => {
        const result = new IrcMessageFactory("").build();
        expect(result).toEqual({
            content: null,
            params: [],
            prefix: null,
            raw: "",
            tags: null,
            type: IRC_TYPES.UNKNOWN,
        });
    });
    test("should create a message with type notice", () => {
        const result = new IrcMessageFactory("").type(IRC_TYPES.NOTICE).build();
        expect(result).toEqual({
            content: null,
            params: [],
            prefix: null,
            raw: "",
            tags: null,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with message apple", () => {
        const result = new IrcMessageFactory("raw").type(IRC_TYPES.NOTICE).content("apple").build();
        expect(result).toEqual({
            content: "apple",
            params: [],
            prefix: null,
            raw: "raw",
            tags: null,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with message apple and a param #test", () => {
        const result = new IrcMessageFactory("raw").type(IRC_TYPES.NOTICE).params(["#test"]).content("apple").build();
        expect(result).toEqual({
            content: "apple",
            params: ["#test"],
            prefix: null,
            raw: "raw",
            tags: null,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with message apple and no params", () => {
        const result = new IrcMessageFactory("raw").type(IRC_TYPES.NOTICE).params().content("apple").build();
        expect(result).toEqual({
            content: "apple",
            params: [],
            prefix: null,
            raw: "raw",
            tags: null,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with no message and no params", () => {
        const result = new IrcMessageFactory("raw").type(IRC_TYPES.NOTICE).params().content().build();
        expect(result).toEqual({
            content: null,
            params: [],
            prefix: null,
            raw: "raw",
            tags: null,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with message apple, tags and a param #test", () => {
        const tags = new Map<string, string>();
        tags.set("mod", "0");
        tags.set("is-mihoka", "yes");
        const result = new IrcMessageFactory("raw")
            .type(IRC_TYPES.NOTICE)
            .params(["#test"])
            .content("apple")
            .tags(tags)
            .build();
        expect(result).toEqual({
            content: "apple",
            params: ["#test"],
            prefix: null,
            raw: "raw",
            tags,
            type: IRC_TYPES.NOTICE,
        });
    });
    test("should create a notice with message apple, tags, prefix and a param #test", () => {
        const tags = new Map<string, string>();
        tags.set("mod", "0");
        tags.set("is-mihoka", "yes");
        const prefix = new Map<string, string>();
        prefix.set("host", "tmi.twitch.tv");
        const result = new IrcMessageFactory("raw")
            .type(IRC_TYPES.NOTICE)
            .params(["#test"])
            .content("apple")
            .tags(tags)
            .prefix(prefix)
            .build();
        expect(result).toEqual({
            content: "apple",
            params: ["#test"],
            prefix,
            raw: "raw",
            tags,
            type: IRC_TYPES.NOTICE,
        });
    });
});
